-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jan 02, 2019 at 10:20 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gdsoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

DROP TABLE IF EXISTS `categorys`;
CREATE TABLE IF NOT EXISTS `categorys` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorys_name_unique` (`category_name`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'জাতীয়', NULL, NULL),
(2, 'আন্তর্জাতিক', NULL, NULL),
(3, 'রাজনীতি', NULL, NULL),
(4, 'অর্থনীতি', NULL, NULL),
(5, 'খেলাধুলা', NULL, NULL),
(6, 'অপরাধ ও দুর্নীতি', NULL, NULL),
(7, 'শিক্ষা', NULL, NULL),
(8, 'বিজ্ঞান ও প্রযুক্তি', NULL, NULL),
(9, 'বিনোদন', NULL, NULL),
(10, 'ফিচার', NULL, NULL),
(11, 'নির্বাচন', NULL, NULL),
(12, 'দুর্ঘটনা', NULL, NULL),
(13, 'সাহিত্য', NULL, NULL),
(14, 'লিড নিউজ', NULL, NULL),
(15, 'উন্নয়ন', NULL, NULL),
(16, 'জেলা সংবাদ', NULL, NULL),
(17, 'কৃষি ও কৃষক', NULL, NULL),
(18, 'বাংলা নববর্ষ', NULL, NULL),
(19, 'অন্যান', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_27_104707_create_prprofiles_table', 2),
(4, '2018_12_31_013631_create_categorys_table', 3),
(5, '2018_12_31_043003_create_posts', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `nameID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `hading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `date`, `nameID`, `categoryID`, `hading`, `created_at`, `updated_at`) VALUES
(1, '2018-12-31', 1, 11, '২৮৮ আসনে মহাজোটের জয়', NULL, NULL),
(2, '2018-12-31', 5, 15, '৮০ শতাংশ ভোট পড়েছে: সিইসি', NULL, NULL),
(3, '2018-12-31', 3, 11, 'ঢাকা-১৯: বেসরকারিভাবে নৌকার ডা. এনাম নির্বাচিত', NULL, NULL),
(4, '2018-12-30', 3, 12, '২০১৮’তে গাইবান্ধায় সড়ক দুর্ঘটনায় নিহত ৪১, আহত ৩৬২', NULL, NULL),
(5, '2018-12-31', 4, 8, 'প্রায় ১০ ঘণ্টা পর থ্রিজি-ফোরজি সেবা চালু', NULL, NULL),
(6, '2018-12-29', 3, 1, 'সবার প্রধানমন্ত্রী হিসেবেই ৫ বছর দায়িত্ব পালন করবেন শেখ হাসিনা', NULL, NULL),
(7, '2019-01-01', 1, 11, '১৮৬ আসনে ৮০ শতাংশের বেশি ভোট', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prprofile`
--

DROP TABLE IF EXISTS `prprofile`;
CREATE TABLE IF NOT EXISTS `prprofile` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prprofile_mobile_unique` (`number`),
  UNIQUE KEY `prprofile_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prprofile`
--

INSERT INTO `prprofile` (`id`, `name`, `designation`, `area`, `address`, `email`, `number`, `active`, `created_at`, `updated_at`) VALUES
(1, 'অরবিন্দ রায়', 'স্টাফ রিপোর্টার', 'নরসিংদী সদর', 'নরসিংদী', NULL, '01918767655', 1, NULL, NULL),
(2, 'মো. কামাল হোসেন ভূঞা', 'স্টাফ ফটোগ্রাফার', 'মনোহরদী', 'মনোহরদী', NULL, '01820501427', 1, NULL, NULL),
(3, 'মোঃ জসিম উদ্দিন', 'চিফ রিপোর্টার', 'নরসিংদী সদর', 'চন্দনপুর পূর্ব পাড়া, মনোহরদী, নরসিংদী', 'jashim4666@gmail.com', '01712416766', 1, NULL, NULL),
(4, 'খন্দকার তৌহিদুল ইসলাম', 'স্টাফ ফটোগ্রাফার', 'নরসিংদী সদর', 'নরসিংদী সদর', NULL, NULL, 1, NULL, NULL),
(5, 'মো. কামাল হোসেন', 'স্টাফ রিপোর্টার', 'নরসিংদী সদর', 'নরসিংদী সদর', NULL, '01718810249', 1, NULL, NULL),
(6, 'আমিনুল হক', 'স্টাফ রিপোর্টার', 'বেলাব', 'বেলাব', 'aminul1986h@gmail.com', '01716360744', 1, NULL, NULL),
(7, 'শাহজালাল হীরা', 'প্রতিনিধি', 'মনোহরদী', 'মনোহরদী', NULL, '01716455655', 1, NULL, NULL),
(8, 'আসাদুজ্জামান নূর', 'বিশেষ প্রতিনিধি', 'মনোহরদী', 'মনোহরদী', 'asadamardesh@gmail.com', '01716018260', 1, NULL, NULL),
(9, 'মো. আনোয়ার হোসেন', 'আঞ্চলিক প্রতিনিধি', 'মনোহরদী', 'মনোহরদী', 'anowarhossain878@gmail.com', '01718884586', 1, NULL, NULL),
(10, 'এস.এম. খোরশেদ আলম', 'শিবপুর প্রতিনিধি', 'শিবপুর', 'শিবপুর', 'smkalam2014@gmail.com', '01716785671', 1, NULL, NULL),
(11, 'একেএম মাসুদ রানা', 'আঞ্চলিক প্রতিনিধি', 'শিবপুর', 'শিবপুর', 'news.masudrana74@gmail.com', '01721624919', 1, NULL, NULL),
(12, 'মো. নজরুল ইসলাম', 'প্রতিনিধি', 'মাধবদী', 'মাধবদী', 'madhabdipressclub@gmail.com', '01718352690', 1, NULL, NULL),
(13, 'মো. মোবারক হোসেন', 'পৌর প্রতিনিধি', 'পলাশ', 'পলাশ', 'mobarak.news2015@gmail.com', '01676808205', 1, NULL, NULL),
(14, 'মাহবুবুল আলম লিটন', 'প্রতিনিধি', 'রায়পুরা', 'রায়পুরা', 'litoninfo3@gmail.com', '01829037230', 1, NULL, NULL),
(15, 'মো. মোস্তফা খান', 'বিশেষ প্রতিনিধি', 'রায়পুরা', 'রায়পুরা', NULL, NULL, 1, NULL, NULL),
(16, 'মাজেদুল ইসলাম', 'বিশেষ প্রতিনিধি', 'রায়পুরা', 'রায়পুরা', NULL, '01718386481', 1, NULL, NULL),
(17, 'মো. আশাদউল্লাহ মনা', 'বিশেষ প্রতিনিধি', 'পলাশ', 'পলাশ', NULL, NULL, 1, NULL, NULL),
(18, 'মো. এমদাদুল হক খোকন', 'বিশেষ প্রতিনিধি', 'মাধবদী', 'মাধবদী', NULL, '01190475084', 1, NULL, NULL),
(19, 'এম.আর. ওয়াসিম', 'প্রতিনিধি', 'ভৈরব', 'ভৈরব,কিশোরগঞ্চ', 'mr.wasim35@gmail.com', '01747999174', 1, NULL, NULL),
(20, 'মো. আল-আমিন মিয়া', 'প্রতিনিধি', 'পলাশ', 'পলাশ', NULL, NULL, 1, NULL, NULL),
(21, 'কাজী আব্দুল্লাহ আল মামুন', 'স্টাফ রিপোর্টার', 'নরসিংদী সদর', 'নরসিংদী সদর', 'kazi.jsl@gmail.com', '01725494949', 1, NULL, NULL),
(22, 'মো. কামরুজ্জামান সরকার', 'স্টাফ রিপোর্টার', 'নরসিংদী সদর', 'নরসিংদী সদর', NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'gdadmin', 'shakhawat.sk@gmail.com', NULL, '$2y$10$RbeiRsChbq8MciiHAAL.f.Iwq/BAcn3Fjt3A5eOgGOQyJ8SySlOiK', 'KASAD3rvXSTqIwnv2C1IkVedUxSpmHryjoI4qdp1mL4LF1mBfnHCGH9ZhoDc', '2018-12-26 09:47:21', '2018-12-26 09:47:21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
