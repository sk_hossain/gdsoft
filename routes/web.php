<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/addMane', 'addPagelink@addProtinidi')->name('addMane');
Route::get('/prlist', 'addPagelink@prlist')->name('prlist');
Route::get('/edit-pr/{id}', 'editdataController@predit')->name('edit-pr');
Route::get('/editnews/{id}', 'editdataController@editnews')->name('editnews');
Route::get('/status-pr/{id}/{active}', 'editdataController@prst')->name('prst');
Route::get('/addnews', 'addPagelink@newsaddform')->name('addnews');
Route::get('/newslist', 'addPagelink@newslist')->name('newslist');
Route::get('/newslist/filter', 'addPagelink@newslist')->name('newslistfilter');
Route::get('/report', 'addPagelink@report')->name('report');
Route::get('/profile/{id}', 'addPagelink@profile')->name('profile');
Route::get('/profile/filter/{id}', 'addPagelink@profile')->name('profile');
Route::get('/report/filter', 'addPagelink@filter')->name('filter');


//Post
Route::post('/pradd', 'insertData@addprsave')->name('pradd');
Route::post('/predit/{id}', 'editdataController@editpr')->name('predit');
Route::post('/editpostnews/{id}', 'editdataController@editpostnews')->name('editpostnews');
Route::post('/newsadd', 'insertData@addnewssave')->name('newsadd');
