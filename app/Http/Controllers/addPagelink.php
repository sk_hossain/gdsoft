<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class addPagelink extends Controller
{
    public function addProtinidi()
    {
        return view('addProtinidi');
    }
    public function prlist()
    {
        $prs = DB::table('prprofile')->get();

        return view('prlist', ['prs' => $prs]);
    }
    
    public function newsaddform(){
        $prs = DB::table('prprofile')->where('active',1)->get();
        $categorys = DB::table('categorys')->get();
        
        return view('addNews',['prs' => $prs,'categorys'=> $categorys]);
    }
    
    //News List
    public function newslist(){
        $posts = DB::table('posts')
            ->join('prprofile', 'prprofile.id', '=', 'posts.nameID')
            ->join('categorys', 'categorys.id', '=', 'posts.categoryID')
            ->select('posts.id','posts.date', 'posts.categoryID', 'posts.hading', 'prprofile.name','posts.nameID','categorys.category_name')
            ->get();
        
        return view('newslist', ['posts' => $posts]);
    }
    
    //report
    public function report(){
        $prs = DB::table('prprofile')->where('active',1)->get();   
        return view('report', ['prs' => $prs]);
      
    }
  public function filter(Request $request){
      $start=$request->start;
      $end=$request->end;
      $posts = DB::table('posts')
          ->whereBetween('date', [$start, $end])
          ->get()->toArray();
      
      $arc=count($posts);
    if(!$arc==0){
      foreach($posts as $post){
          $nameid[]=$post->nameID;
      }
      $prnameid=array_unique($nameid);
    
      $prs = DB::table('prprofile')
            ->where('active',1)
            ->whereIn('id', $prnameid)
            ->get();
        return view('report', ['prs' => $prs]);
    }
      else{
          $prs=array();
          return view('report', ['prs' => $prs]);
      }
  }
    
    //Profile
    public function profile($id){
        $prs = DB::table('prprofile')->where([['active','=',1],['id','=',$id]])->get();   
        return view('Profile', ['prs' => $prs,'id'=>$id]);
    } 
    
}
