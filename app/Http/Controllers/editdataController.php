<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class editdataController extends Controller
{
    public function predit($id){
        $spr=DB::table('prprofile')->where('id',$id)->first();
        return view('editProtinidi')->with('singl_pr',$spr);
    }
  
    public function editpr(Request $request,$id){
        $validation =$request->validate([
            'name'=> 'required | min:3',
            'designation'=> 'required',
            'area'=> 'required',
            'address'=> 'required',
            'email'=>'Nullable | E-Mail',
            'number'=>'Nullable | Numeric'
        ]);
        
        print_r($validation);
        $data=array();
        $data['name']=$request->name;
        $data['designation']=$request->designation;
        $data['area']=$request->area;
        $data['address']=$request->address;
        $data['email']=$request->email;
        $data['number']=$request->number;
        $insert=DB::table('prprofile')->where('id',$id)->update($data);
        if($insert){
            $notification=array(
                'messege'=>'আপনি সফল ভাবে প্রতিনিধির তথ্য পরিবর্তন করেছেন।',
                'alert-type'=>'success'
            );
            return Redirect()->Route('prlist')->with($notification);
        }else{
           $notification=array(
                'messege'=>'আপনি সফল ভাবে প্রতিনিধির তথ্য পরিবর্তন করতে পারেন নাই।',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }
    
    public function prst($id,$active){
        if($active==1){
            $data['active']=0;
        $insert=DB::table('prprofile')->where('id',$id)->update($data);
        return Redirect()->back();
        }else{
            $data['active']=1;
        $insert=DB::table('prprofile')->where('id',$id)->update($data);
        return Redirect()->back();
        }
    }
    
    public function editnews($id){
            $posts = DB::table('posts')->where('id',$id)->get();
            $prs = DB::table('prprofile')->where('active',1)->get();
            $categorys = DB::table('categorys')->get(); 
        
        return view('editnews', ['posts' => $posts,'prs' => $prs,'categorys'=> $categorys]);
    }
    
    //Edit Post
    public function editpostnews(Request $request,$id){
          $validation =$request->validate([
            'date'=> 'Required | Date',
            'nameID'=> 'Required | Numeric',
            'categoryID'=> 'Required | Numeric',
            'hading'=> 'Required',
        ]);
        
        $data=array();
        $data['date']=$request->date;
        $data['nameID']=$request->nameID;
        $data['categoryID']=$request->categoryID;
        $data['hading']=$request->hading;
        $edit=DB::table('posts')->where('id',$id)->update($data);
        if($edit){
            $notification=array(
                'messege'=>'আপনি সফল ভাবে সংবাদ সংশোধন করেছেন।',
                'alert-type'=>'success'
            );
            return Redirect()->Route('newslist')->with($notification);
        }else{
           $notification=array(
                'messege'=>'আপনি সফল ভাবে সংবাদ সংশোধন করতে পারেন নাই।',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }
}
