<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class insertData extends Controller
{
    public function addprsave(Request $request){
        $validation =$request->validate([
            'name'=> 'required | min:3',
            'designation'=> 'required',
            'area'=> 'required',
            'address'=> 'required',
            'email'=>'Nullable | E-Mail',
            'number'=>'Nullable | Numeric'
        ]);
        
        $data=array();
        $data['name']=$request->name;
        $data['designation']=$request->designation;
        $data['area']=$request->area;
        $data['address']=$request->address;
        $data['email']=$request->email;
        $data['number']=$request->number;
        $insert=DB::table('prprofile')->insert($data);
        if($insert){
            $notification=array(
                'messege'=>'আপনি সফল ভাবে প্রতিনিধি যোগ করেছেন।',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
           $notification=array(
                'messege'=>'আপনি সফল ভাবে প্রতিনিধি যোগ করতে পারেন নাই।',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }
    
    public function addnewssave(Request $request){
       $validation =$request->validate([
            'date'=> 'Required | Date',
            'nameID'=> 'Required | Numeric',
            'categoryID'=> 'Required | Numeric',
            'hading'=> 'Required',
        ]);
        
        $data=array();
        $data['date']=$request->date;
        $data['nameID']=$request->nameID;
        $data['categoryID']=$request->categoryID;
        $data['hading']=$request->hading;
        $insert=DB::table('posts')->insert($data);
        if($insert){
            $notification=array(
                'messege'=>'আপনি সফল ভাবে সংবাদ সংরক্ষণ করেছেন।',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
           $notification=array(
                'messege'=>'আপনি সফল ভাবে সংবাদ সংরক্ষণ করতে পারেন নাই।',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }
}
