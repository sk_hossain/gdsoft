@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                    <div class="col-md-2">
                        <a href="{{ route('addMane') }}"><button class="btn btn-warning">প্রতিনিধি যোগ করা</button></a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('prlist') }}"><button class="btn btn-warning">প্রতিনিধি তালিকা</button></a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('addnews') }}"><button class="btn btn-warning">প্রাপ্ত সংবাদ সংরক্ষণ</button></a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('newslist') }}"><button class="btn btn-warning">প্রাপ্তি সংবাদের তালিকা</button></a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('report') }}"><button class="btn btn-warning">প্রাপ্তি সংবাদের রিপোর্ট</button></a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
