@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>->প্রতিনিধিদের তালিকা <button class="float-right" onclick="myFunction()">Print</button></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <div class="col-md-12>">
                    <div id="headerprint">
                        <img style="display: block; margin: auto; width:25%;" src="{{ asset('img/Logo-1024x372.jpg') }}" alt="LOGO">
                            <p style="text-align: center;">অফিসঃ হাজী আব্দুল গফুর মার্কেট, উপজেলা মোড়, কোর্ট রোড, নরসিংদী-১৬০২<br>
                        বিজ্ঞাপন বিভাগ ফোনঃ- 02-9451782, মোবাইল: 01867-379991, 01716-288845 <br> 
                        ওয়েবঃ- www.dailygrameendarpan.com </p>
                        <?php if(isset($_GET['start'])){
                    echo date("d-m-Y", strtotime($_GET['start'])); 
                    echo " থেকে "; echo date("d-m-Y", strtotime($_GET['end']));} ?>
                    </div>
                      <table class="table table-striped">
                          <tr>
                          <th>ক্র: নং</th>
                          <th>নাম</th>
                          <th>পদবী</th>
                          <th>এলাকা</th>
                          <th>ঠিকানা</th>
                          <th>ই-মেইল</th>
                          <th>মোবাইল</th>
                          <th>স্ট্যাটাস</th>
                          <th>এডিট</th>
                          </tr>
                          <?php $listcount=1; ?>
                    @foreach ($prs as $pr)
                          <tr>
                        <td><?php  echo $listcount; $listcount++;?></td>
                              <td><a href="{{URL::to('profile/'.$pr->id)}}">{{$pr->name}}</a></td>
                          <td>{{$pr->designation}}</td>
                          <td>{{$pr->area}}</td>
                          <td>{{$pr->address}}</td>
                          <td>{{$pr->email}}</td>
                          <td>{{$pr->number}}</td>
                          <td>
                              
                              @if($pr->active==1) সক্রিয় @else নিষ্ক্রিয় @endif <br>
                              @if($pr->active==1) <a href="{{URL::to('status-pr/'.$pr->id.'/'.$pr->active)}}">নিষ্ক্রিয় করুন </a> @else <a href="{{URL::to('status-pr/'.$pr->id.'/'.$pr->active)}}">সক্রিয় করুন </a> @endif
                              </td>
                              <td><a href="{{URL::to('edit-pr/'.$pr->id)}}"><button class="btn btn-info">Edit</button></a></td>
                          </tr>
                    @endforeach
                      </table>
                    </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
