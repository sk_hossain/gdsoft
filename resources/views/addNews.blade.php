@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>-> সংবাদ সংরক্ষণ </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   @if($errors->any())
                        <div class="col-md-12 text-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>    
                            @endforeach
                        </ul>
                        </div>
                        @endif
                    <div class="col-md-8 offset-2">
                        <form action="{{ route('newsadd') }}" method="POST" class="form-group">
                          @csrf
                            <div class="form-group">
                                <label> তারিখ </label>
                                 <input type="date" name="date" value="{{ date('y-m-d') }}" class="form-control" required>
                            </div>
                       <div class="form-group">
                           <label> সাংবাদিকের নাম </label>
                        <select name="nameID" class="form-control form-control-lg" required>
                            <option value=""> নির্বাচন করুন </option>
                             @foreach ($prs as $pr)
                            <option value="{{$pr->id}}">{{$pr->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="form-group">
                            <label> সংবাদ ক্যাটাগরি  </label>
                        <select name="categoryID" class="form-control form-control-lg" required>
                            <option value=""> নির্বাচন করুন </option>
                             @foreach ($categorys as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select> 
                        </div>
                        <div class="form-group">
                            <label> সংবাদ শিরনাম </label>
                        <input type="text" name="hading" placeholder="সংবাদ শিরনাম" class="form-control" required>
                        </div> 
                            <input type="submit" value="Save">
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
