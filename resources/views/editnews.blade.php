@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>-> <a href="{{URL::to('newslist')}}"> সংবাদের তালিকা </a> ->  সংবাদ সংশোধন </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   @if($errors->any())
                        <div class="col-md-12 text-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>    
                            @endforeach
                        </ul>
                        </div>
                        @endif
                    <div class="col-md-8 offset-2">
                        @foreach ($posts as $post)
                        <form action="{{ URL::to('editpostnews/'.$post->id) }}" method="POST" class="form-group">
                          @csrf
                            
                            <div class="form-group">
                                <label> তারিখ </label>
                               <input type="date" name="date" value="{{$post->date}}" class="form-control" required>
                            </div>
                       <div class="form-group">
                           <label> সাংবাদিকের নাম </label>
                        <select name="nameID" class="form-control form-control-lg" required>
                             @foreach ($prs as $pr)
                            <option value="{{$pr->id}}" 
                                   
                                @if ($pr->id === $post->nameID)
                                selected
                                @endif
                                >{{$pr->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="form-group">
                            <label> সংবাদ ক্যাটাগরি  </label>
                        <select name="categoryID" class="form-control form-control-lg" required>
                             @foreach ($categorys as $category)
                            <option value="{{$category->id}}"
                                    
                                @if ($category->id === $post->categoryID)
                                selected
                                @endif
                                
                                    >{{$category->category_name}}</option>
                            @endforeach
                        </select> 
                        </div>
                        <div class="form-group">
                            <label> সংবাদ শিরনাম </label>
                        <input type="text" name="hading" 
                               value="{{$post->hading}}" class="form-control" required>
                        </div>
                            <input type="submit" value="Save">
                        </form>
                        @endforeach
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
