@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>->সংবাদের তালিকা 
                     <form type="GET" action="{{URL::to('newslist/filter')}}" class="form-control">
                    <input type="date" name="start" @if(isset($_GET['start'])) value="{{$_GET['start']}}" @endif required> 
                    TO <input type="date" name="end" @if(isset($_GET['end'])) value="{{$_GET['end']}}" @endif required> 
                    <input type="submit" value="Filter">
                </form>
                    <button class="float-right" onclick="myFunction()">Print</button></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <div class="col-md-12>">
                    <div id="headerprint">
                        <img style="display: block; margin: auto; width:25%;" src="{{ asset('img/Logo-1024x372.jpg') }}" alt="LOGO">
                            <p style="text-align: center;">অফিসঃ হাজী আব্দুল গফুর মার্কেট, উপজেলা মোড়, কোর্ট রোড, নরসিংদী-১৬০২<br>
                        বিজ্ঞাপন বিভাগ ফোনঃ- 02-9451782, মোবাইল: 01867-379991, 01716-288845 <br> 
                        ওয়েবঃ- www.dailygrameendarpan.com </p>
                        <?php if(isset($_GET['start'])){
                    echo date("d-m-Y", strtotime($_GET['start'])); 
                    echo " থেকে "; echo date("d-m-Y", strtotime($_GET['end']));} ?>
                    </div>
                      <table class="table table-striped">
                          <tr>
                          <th>ক্র: নং</th>
                        <th>তারিখ</th>
                          <th>নাম</th>
                          <th>ক্যাটাগরি</th>
                          <th>সংবাদ শিরনাম</th>
                          <th>Edit</th>
                          </tr>
                          <?php $listcount=1; ?>
                    <?php 
                        if(isset($_GET['start'])){
                            $flposts=flposts($_GET['start'],$_GET['end']);
                     foreach($flposts as $flpost){
                         ?>
                         <tr>
                        <td><?php  echo $listcount; $listcount++;?></td>
                          <td>{{date("d-m-Y", strtotime($flpost->date))}}</td>
                              <td><a href="{{URL::to('profile/'.$flpost->nameID)}}">{{$flpost->name}} </a></td>
                          <td>{{$flpost->category_name}}</td>
                          <td>{{$flpost->hading}}</td>
                          <td><a href="{{URL::to('editnews/'.$flpost->id)}}"><button class="btn btn-info">Edit</button></a></td>
                          </tr>
                          <?php
                     }
                    }else{
                            ?>
                          @foreach ($posts as $post)
                          <tr>
                        <td><?php  echo $listcount; $listcount++;?></td>
                          <td>{{date("d-m-Y", strtotime($post->date))}}</td>
                              <td><a href="{{URL::to('profile/'.$post->nameID)}}">{{$post->name}} </a></td>
                          <td>{{$post->category_name}}</td>
                          <td>{{$post->hading}}</td>
                          <td><a href="{{URL::to('editnews/'.$post->id)}}"><button class="btn btn-info">Edit</button></a></td>
                          </tr>
                    @endforeach
                    <?php
                    }
                    ?>
                    
                      </table>
                    </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
