@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>-> নতুন প্রতিনিধি</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   @if($errors->any())
                        <div class="col-md-12 text-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>    
                            @endforeach
                        </ul>
                        </div>
                        @endif
                    <div class="col-md-8 offset-2">
                        <form action="{{ route('pradd') }}" method="POST" class="form-group">
                          @csrf
                        <input type="text" name="name" placeholder="নাম" class="form-control" required>
                            <br>
                        <input type="text" name="designation" placeholder="পদবী" class="form-control" required>
                            <br>
                        <input type="text" name="area" placeholder="এলাকা" class="form-control" required>
                            <br>
                        <input type="text" name="address" placeholder="ঠিকানা" class="form-control">
                            <br>
                        <input type="text" name="email" placeholder="ই-মেইল" class="form-control">
                            <br>
                        <input type="text" name="number" placeholder="মোবাইল" class="form-control">
                            <br>
                            <input type="submit" value="Submit">
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
