@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>->প্রাপ্তি সংবাদের রিপোর্ট
                <form type="GET" action="{{URL::to('report/filter')}}" class="form-control">
                    <input type="date" name="start" @if(isset($_GET['start'])) value="{{$_GET['start']}}" @endif required> 
                    TO <input type="date" name="end" @if(isset($_GET['end'])) value="{{$_GET['end']}}" @endif required> 
                    <input type="submit" value="Filter">
                </form>
                    <button class="float-right" onclick="myFunction()">Print</button>
                </div>
                <?php if(isset($_GET['start'])){$start=$_GET['start']; $end=$_GET['end'];} ?>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  
                   
                  <div class="col-md-12>">
                    <div id="headerprint">
                        <img style="display: block; margin: auto; width:25%;" src="{{ asset('img/Logo-1024x372.jpg') }}" alt="LOGO">
                            <p style="text-align: center;">অফিসঃ হাজী আব্দুল গফুর মার্কেট, উপজেলা মোড়, কোর্ট রোড, নরসিংদী-১৬০২<br>
                        বিজ্ঞাপন বিভাগ ফোনঃ- 02-9451782, মোবাইল: 01867-379991, 01716-288845 <br> 
                        ওয়েবঃ- www.dailygrameendarpan.com </p>
                        <?php if(isset($_GET['start'])){
                    echo date("d-m-Y", strtotime($_GET['start'])); 
                    echo " থেকে "; echo date("d-m-Y", strtotime($_GET['end']));} ?>
                    </div>
                      <table class="table table-striped">
                          <tr>
                          <th>ক্র: নং</th>
                          <th>নাম</th>
                          <th>পদবী</th>
                          <th>সংবাদ ক্যাটাগরি</th>
                          <th>মোট সংবাদ</th>
                          </tr>
                          
                          <?php $listcount=1; ?>
                    @foreach ($prs as $pr)
                          <tr>
                        <td><?php  echo $listcount; $listcount++;?></td>
                              <td><a href="{{URL::to('profile/'.$pr->id)}}">{{$pr->name}}</a></td>
                          <td>{{$pr->designation}}</td>
                          <td>
                              @if(!isset($_GET['start']))
                              <?php $cats=(get_posts($pr->id));
                               if($cats==null){
                                  $as=($cats[1]);
                               }else{
                                   $as=array_values($cats[1]);
                               }
                            
                             $lan=count($as); 
                              ?>
                              <table class="table table-striped">
                              <tr><td>ক্যাটাগরির নাম</td><td>মোট</td></tr>
                                  
                            @for($i=0;$i<=$lan-1;$i++)
                              <tr><td><?php echo get_category($as[$i]); ?></td><td><?php echo count_cat_posts($as[$i],$pr->id); ?></td></tr>
                              @endfor
                      </table>
                              
                            </td>
                              <td>{{$cats[0]}}</td>
                          @else
                                <?php $cats=(get_posts_withfilter($pr->id,$start,$end));
                              
                            if($cats==null){
                                  $as=($cats[1]);
                               }else{
                                   $as=array_values($cats[1]);
                               }
                            $lan=count($as);
                              ?>
                              <table class="table table-striped">
                              <tr><td>ক্যাটাগরির নাম</td><td>মোট</td></tr>
                                  
                            @for($i=0;$i<=$lan-1;$i++)
                              <tr><td><?php echo get_category($as[$i]); ?></td><td><?php echo count_cat_posts_withfilter($as[$i],$pr->id,$start,$end); ?></td></tr>
                              @endfor
                      </table>
                              
                            </td>
                              <td>{{$cats[0]}}</td>
                @endif
                          </tr>
                    @endforeach
            <tr style="background-color:#b0d7f4"><td colspan="4" align="right">সর্বমোট সংবাদ</td>
                <td>@if(isset($_GET['start']))
                    {{totalpost_withdate($start, $end)}} @else
                    {{totalpost()}}
                    @endif
                </td>
            </tr>
                      </table>
                    </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
