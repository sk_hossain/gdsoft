@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>->প্রতিনিধি প্রোফাইল
                <form type="GET" action="{{URL::to('profile/filter/'.$id)}}" class="form-control">
                    <input type="date" name="start" @if(isset($_GET['start'])) value="{{$_GET['start']}}" @endif required> 
                    TO <input type="date" name="end" @if(isset($_GET['end'])) value="{{$_GET['end']}}" @endif required> 
                    <input type="submit" value="Filter">
                </form>
                    <button class="float-right" onclick="myFunction()">Print</button>
                </div>
                <?php if(isset($_GET['start'])){$start=$_GET['start']; $end=$_GET['end'];} ?>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  
                   
                  <div class="col-md-12>">
                    <div id="headerprint">
                        <img style="display: block; margin: auto; width:25%;" src="{{ asset('img/Logo-1024x372.jpg') }}" alt="LOGO">
                            <p style="text-align: center;">অফিসঃ হাজী আব্দুল গফুর মার্কেট, উপজেলা মোড়, কোর্ট রোড, নরসিংদী-১৬০২<br>
                        বিজ্ঞাপন বিভাগ ফোনঃ- 02-9451782, মোবাইল: 01867-379991, 01716-288845 <br> 
                        ওয়েবঃ- www.dailygrameendarpan.com </p>
                        <?php if(isset($_GET['start'])){
                    echo date("d-m-Y", strtotime($_GET['start'])); 
                    echo " থেকে "; echo date("d-m-Y", strtotime($_GET['end']));} ?>
                    </div>
                      <div class="row">
                      <div class="prinfo col-md-5">
                      <table class="table table-striped">
                          @foreach($prs as $pr)
                              <tr><td>নাম</td><td>{{$pr->name}}</td></tr>
                              <tr><td>পদবী</td><td>{{$pr->designation}}</td></tr>
                              <tr><td>এলাকা</td><td>{{$pr->area}}</td></tr>
                              <tr><td>ঠিকানা</td><td>{{$pr->address}}</td></tr>
                              <tr><td>মোবাইল</td><td>{{$pr->number}}</td></tr>
                              <tr><td>ই-মেইল</td><td>{{$pr->email}}</td></tr>
                          @endforeach
                        </table>
                      </div>
                      <div class="com-md-7">
                          <table class="table table-striped bg-info">
                          <tr>
                          <th>সংবাদ ক্যাটাগরি</th>
                          <th>মোট সংবাদ</th>
                          </tr>
                            <tr>
                                <td>
                                    @foreach ($prs as $pr)
                                    @if(!isset($_GET['start']))
                                        <?php $cats=(get_posts($pr->id));
                                    
                                        if($cats==null){
                                            $as=($cats[1]);
                                        }else{
                                            $as=array_values($cats[1]);
                                        }
                                            $lan=count($as);
                                        ?>
                                    <table class="table table-striped">
                                        <tr><td>ক্যাটাগরির নাম</td><td>মোট</td></tr>
                                  
                                     @for($i=0;$i<=$lan-1;$i++)
                                        <tr><td><?php echo get_category($as[$i]); ?></td><td><?php echo count_cat_posts($as[$i],$pr->id); ?></td></tr>
                                    @endfor
                                    </table>
                              
                            </td>
                              <td>{{$cats[0]}}</td>
                            
                            </tr>  
                      @else
                      <?php $cats=(get_posts_withfilter($pr->id,$start,$end));
                              
                            $as=($cats[1]);
                            $lan=count($as);
                              ?>
                              <table class="table table-striped">
                              <tr><td>ক্যাটাগরির নাম</td><td>মোট</td></tr>
                                  
                            @for($i=0;$i<=$lan-1;$i++)
                              <tr><td><?php echo get_category($as[$i]); ?></td><td><?php echo count_cat_posts_withfilter($as[$i],$pr->id,$start,$end); ?></td></tr>
                              @endfor
                      </table>
                              
                            </td>
                              <td>{{$cats[0]}}</td>
                @endif
                      
                       @endforeach
                   
                          </table>
                      </div>
                    </div>
</div>
    </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
