@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="{{URL::to('/')}}">Dashboard</a>-> <a href="{{URL::to('/prlist')}}">প্রতিনিধিদের তালিকা</a>-> প্রতিনিধি তথ্য পরিবর্তন  </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   @if($errors->any())
                        <div class="col-md-12 text-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>    
                            @endforeach
                        </ul>
                        </div>
                        @endif
                    <div class="col-md-8 offset-2">
                        <form action="{{ URL::to('predit/'.$singl_pr->id) }}" method="POST" class="form-group">
                          @csrf
                        <input type="text" name="name" value="{{$singl_pr->name}}" class="form-control" required>
                            <br>
                        <input type="text" name="designation" value="{{$singl_pr->designation}}" class="form-control" required>
                            <br>
                        <input type="text" name="area" value="{{$singl_pr->area}}" class="form-control" required>
                            <br>
                        <input type="text" name="address" value="{{$singl_pr->address}}" class="form-control">
                            <br>
                        <input type="text" name="email" value="{{$singl_pr->email}}" class="form-control">
                            <br>
                        <input type="text" name="number" value="{{$singl_pr->number}}" class="form-control">
                            <br>
                            <input type="submit" value="Submit">
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
